#Resources Reservation Platform

##Website
* https://ost-reservations-platform.appspot.com/

##Prerequisites
* google appengine api
* python2.7
* webapp2
* jinja2

##Usage
* To use this platform, user must log in with a Google account. Multiple users can log in and operate at the same time.

* There two search approach, by resource name or by resource start time with a duration. For searching by resource name, only resource whose name is eactly same as searching name are selected. For searching by resource start time, all the resources that have avaiable time in the search time period are selected.

* Resource name is not unique.

* Resource available time cannot span a day.

* Reserving resources only need to specify the start time and duration of the reservation.

* All the resouces including passed ones (current time exceeds resource end time) will be showed to user, so if user tries to reserve a passed resouce, system will not accept this action and return an error message.

* A resource can be reserved more than one reservation at a time/period. The capacity of resources is the number that how many reservations can be made at the same time/period. If user tries to reserve a resource whose reservations at this time/period exceeds the capacity, system will not accept this action and return an error message. 

* User can check all the reservations made for a resource by clicking the resource name displayed on any page.

* A resource reserved time is showed on its resource page.

* User can view all the reservations made and resources added by a user by clicking the user displayed on the resource page

* All the reservations made for a resource will be deleted after this resource is modified, in case of the available time or capacity is not suitable for those reservations.

* Deleted reservations will be removed from database and not be showed in any where.

* Rss will generate all the reservations except the deleted ones (including the past ones).

* User can view all the resouces with a tag by clicking the tag displayed under resoures sections.

* User will receive a email confirmation when reserving a resource successfully.

* User can upload an image when creating or editing a resource. But the image is just stored in database, not showed in the platform.

* There is a branch of GIT code with the tag "experiment" that turns off the rss function.


##Author
* [Nan Liu](nl1554@nyu.edu)(nl1554@nyu.edu)

