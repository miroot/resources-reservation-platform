#!/usr/bin/env python

# Nan Liu
# N11508213

import os
import urllib
import datetime
import uuid

from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.api import images
from google.appengine.api import mail


import jinja2
import webapp2


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


DEFAULT_KEY = 'default'


def getCurrentTime():
    currenttime = datetime.datetime.now() - datetime.timedelta(hours = 4)
    current_time = datetime.datetime(currenttime.year, currenttime.month, currenttime.day, currenttime.hour, currenttime.minute)
    return current_time


# [START reservations]
def reservation_key(user_email):
    return ndb.Key('Reservations', user_email)

class Reservations(ndb.Model):
    reservation_id = ndb.StringProperty(indexed=True, required=True)
    reservation_user = ndb.StringProperty(indexed=False)
    resource_id = ndb.StringProperty(indexed=True)
    resource_name = ndb.StringProperty(indexed=False)
    resource_owner = ndb.StringProperty(indexed=False)
    start_time = ndb.DateTimeProperty(indexed=True, required=True)
    duration = ndb.StringProperty(indexed=False)
    date_str = ndb.StringProperty(indexed=False)
    start_str = ndb.StringProperty(indexed=False)
# [END reservations]


# [START get_reservations]
def hidePassedReservation(reservations_query):
    current_time = getCurrentTime()
    reservations = []
    for r in reservations_query:
        end_time = r.start_time + datetime.timedelta(minutes=int(r.duration))
        if current_time >= end_time:
            continue
        reservations.append(r)

    return reservations


def getUserReservations(user_email):
    # get reservations made for resources by that user, sorted by reservation time
    reservations_query = Reservations.query(ancestor=reservation_key(user_email))
    reservations_query = reservations_query.order(Reservations.start_time).fetch()

    reservations = hidePassedReservation(reservations_query)

    return reservations


def getResouceReservations(resource_id):
    resource_reservations_query = Reservations.query(Reservations.resource_id == resource_id)
    resource_reservations_query = resource_reservations_query.order(Reservations.start_time).fetch()

    resource_reservations = hidePassedReservation(resource_reservations_query)

    return resource_reservations
# [END get_reservations]


# [START resources]
def resource_key():
    return ndb.Key('Resources', DEFAULT_KEY)

class Resources(ndb.Model):
    # 4 & 5. 
    resource_id = ndb.StringProperty(indexed=True, required=True)
    resource_owner = ndb.StringProperty(indexed=True)
    resource_name = ndb.StringProperty(indexed=True)
    availability = ndb.StringProperty(indexed = False)
    start_time = ndb.DateTimeProperty(indexed = False)
    end_time = ndb.DateTimeProperty(indexed = False)
    date_str = ndb.StringProperty(indexed = False)
    start_str = ndb.StringProperty(indexed = False)
    end_str = ndb.StringProperty(indexed = False)
    tags = ndb.StringProperty(indexed=False, repeated = True)
    capacity = ndb.IntegerProperty(indexed=False)
    image_id = ndb.StringProperty(indexed=True)
    image = ndb.BlobProperty(indexed=False)
    avatar = ndb.BlobProperty(indexed=False)
    description = ndb.StringProperty(indexed=False)
    reservations = ndb.IntegerProperty(indexed=False)
    last_reserve_time = ndb.DateTimeProperty(indexed=True)
# [END resources]


# [START get_resources]
def getAvailableResources():
    # all resources in the system, shown in reverse time order based on last made reservation
    resources_query = Resources.query(ancestor=resource_key())
    resources = resources_query.order(-Resources.last_reserve_time).fetch()
    return resources

def getUserOwnsResources(user):
    # resources that the user owns, each linked to its URL
    resources_query = Resources.query(Resources.resource_owner == user)
    resources = resources_query.order(-Resources.last_reserve_time).fetch()
    return resources

def getPerResource(resource_id):
    resource = Resources.query(Resources.resource_id == resource_id).fetch()
    return resource
# [END get_resources]


# [START SearchByName]
class SearchByName(webapp2.RequestHandler):
    def get(self):
        user = self.request.get('user')
        url = users.create_logout_url(self.request.uri)
        url_linktext = 'Logout'

        search_resource_name = self.request.get('search_resource_name')
        search_resource_name_arr = search_resource_name.split('+')
        search_resource_name = ' '.join(search_resource_name_arr)
        resources_query = Resources.query(Resources.resource_name == search_resource_name)
        resources = resources_query.fetch()

        template_values = {
            'user': user,
            'url': url,
            'url_linktext': url_linktext,
            'resources': resources,
        }

        template = JINJA_ENVIRONMENT.get_template('templates/searchbyname.html')
        self.response.write(template.render(template_values))

# [END SearchByName]



# [START SearchByTime]
class SearchByTime(webapp2.RequestHandler):
    def get(self):
        user = self.request.get('user')
        url = users.create_logout_url(self.request.uri)
        url_linktext = 'Logout'

        search_start_date_str = self.request.get('search_start_date')
        search_start_date = search_start_date_str.split('-')
        search_start_time_str = self.request.get('search_start_time')
        search_start = search_start_time_str.split(':')
        search_duration = self.request.get('search_duration')

        search_start_time = datetime.datetime(int(search_start_date[0]), int(search_start_date[1]), int(search_start_date[2]), int(search_start[0]), int(search_start[1]))
        search_end_time = search_start_time + datetime.timedelta(minutes=int(search_duration))


        resources = []
        all_resources = getAvailableResources()
        for r in all_resources:
            if (r.start_time >= search_start_time and r.start_time <= search_end_time) or (r.end_time >= search_start_time and r.end_time <= search_end_time):
                resources.append(r)


        template_values = {
            'user': user,
            'url': url,
            'url_linktext': url_linktext,
            'resources': resources,
        }

        template = JINJA_ENVIRONMENT.get_template('templates/searchbytime.html')
        self.response.write(template.render(template_values))

# [END SearchByTime]


# [START send_mail_confirmation]
def sendEmailConfirmation(reservation):
    mail.send_mail(sender = "nl1554@nyu.edu",
                   to= users.get_current_user().email(),
                   subject="You just successfully made a reservation!",
                   body="Hi,\n\n" + "\tYour " + reservation.reservation_user + " account has reserved " + reservation.resource_name +".  We are appreciately for your support and you can now visit our Resources Reservations Platform for further features.\n\n\tPlease let us know if you have any questions.\n\nThe Resources Reservations Platform Team")
# [END send_mail_confirmation]


# [START add_reservation]
class AddReservation(webapp2.RequestHandler):
    def get(self):
        user = self.request.get('user')
        url = users.create_logout_url(self.request.uri)
        url_linktext = 'Logout'

        resource_id = self.request.get('resourceid')
        resources = getPerResource(resource_id)
        resource = resources[0]

        template_values = {
            'user': user,
            'url': url,
            'url_linktext': url_linktext,
            'resource_id': resource_id,
            'resource_name': resource.resource_name,
            'start_date': resource.date_str,
            'reservation_start_time': resource.start_str
        }

        template = JINJA_ENVIRONMENT.get_template('templates/addreservation.html')
        self.response.write(template.render(template_values))


    def post(self):
        template = JINJA_ENVIRONMENT.get_template('templates/addreservation.html')
        template_values = dict()

        user = self.request.get('user')
        url = self.request.get('url')
        url_linktext = self.request.get('url_linktext')

        resource_id = self.request.get('resourceid')
        resources = getPerResource(resource_id)
        resource = resources[0]


        #reservation_date_str = self.request.get('start_date')
        reservation_date_str = resource.date_str
        reservation_date = reservation_date_str.split('-')
        reservation_start_str = self.request.get('reservation_start_time')
        reservation_start = reservation_start_str.split(':')
        duration = self.request.get('duration')


        reservation_start_time = datetime.datetime(int(reservation_date[0]), int(reservation_date[1]), int(reservation_date[2]), int(reservation_start[0]), int(reservation_start[1]))
        reservation_end_time = reservation_start_time + datetime.timedelta(minutes=int(duration))


        flag = ""
        MSG = ""
 
        current_time = getCurrentTime()
        if reservation_start_time < resource.start_time or reservation_start_time >= resource.end_time or reservation_end_time < resource.start_time or reservation_end_time >= resource.end_time or current_time >= resource.end_time:
            flag = "error"
            MSG = "Please choose valid start time and duration!"
        else:
            resource_reservations = getResouceReservations(resource_id)
            reserved_times = 0
            for r in resource_reservations:
                end_time = r.start_time + datetime.timedelta(minutes=int(r.duration))
                if (reservation_start_time >= r.start_time and reservation_start_time <= end_time) or (reservation_end_time >= r.start_time and reservation_end_time <= end_time):
                    reserved_times += 1

            if reserved_times > resource.capacity:
                flag = "error"
                MSG = "Available resources at this period are reserved out. Please choose another time interval!"
            else:
                flag = "success"
                MSG = "Resource reserved!"


        if flag == "success":
            reservation = Reservations(parent=reservation_key(users.get_current_user().email()))
            reservation.reservation_id = str(uuid.uuid4())
            reservation.reservation_user = users.get_current_user().email()
            reservation.resource_id = resource_id
            reservation.resource_name = resource.resource_name
            reservation.resource_owner = resource.resource_owner
            reservation.start_time = reservation_start_time
            reservation.duration = duration
            reservation.date_str = resource.date_str
            reservation.start_str = reservation_start_str
            reservation.put()

            resource.reservations = resource.reservations + 1
            resource.last_reserve_time = getCurrentTime()
            resource.put()

            sendEmailConfirmation(reservation)


        template_values = {
            'user': user,
            'url': url,
            'url_linktext': url_linktext,
            'resource_id': resource_id,
            'resource_name': resource.resource_name,
            'start_date': resource.date_str,
            'reservation_start_time': reservation_start_time,
            'flag' : flag,
            'msg': MSG
        }
        self.response.write(template.render(template_values))

# [END add_reservation]


# [START delete_reservation]
class DeleteReservation(webapp2.RequestHandler):
    def get(self):
        user = self.request.get('user')
        url = users.create_logout_url(self.request.uri)
        url_linktext = 'Logout'

        reservation_id = self.request.get('reservationid')
        reservations = Reservations.query(Reservations.reservation_id == reservation_id).fetch()
        reservation = reservations[0]

        template_values = {
            'user': user,
            'url': url,
            'url_linktext': url_linktext,
            'reservation_id': reservation_id,
            'resource_id': reservation.resource_id,
            'resource_name': reservation.resource_name,
            'reservation_user': reservation.reservation_user,
            'date_str': reservation.date_str,
            'start_time': reservation.start_str,
            'duration': reservation.duration
        }

        template = JINJA_ENVIRONMENT.get_template('templates/deletereservation.html')
        self.response.write(template.render(template_values))


    def post(self):
        user = self.request.get('user')
        url = users.create_logout_url(self.request.uri)
        url_linktext = 'Logout'

        reservation_id = self.request.get('reservationid')
        resource_id = self.request.get('resourceid')
        reservations = Reservations.query(Reservations.reservation_id == reservation_id).fetch()
        for r in reservations:
            r.key.delete()

        resources = getPerResource(resource_id)
        resource = resources[0]
        resource.reservations -= 1
        resource.put()

        flag = "success"
        MSG = "Reservation deleted!"

        template_values = {
            'user': user,
            'url': url,
            'url_linktext': url_linktext,
            'flag': flag,
            'MSG': MSG
        }

        template = JINJA_ENVIRONMENT.get_template('templates/deletereservation.html')
        self.response.write(template.render(template_values))
# [END delete_reservation]


# [START add_resource]
class AddResource(webapp2.RequestHandler):
    def get(self):
        user = self.request.get('user')
        url = users.create_logout_url(self.request.uri)
        url_linktext = 'Logout'

        template_values = {
            'user': user,
            'url': url,
            'url_linktext': url_linktext,
        }

        template = JINJA_ENVIRONMENT.get_template('templates/addresource.html')
        self.response.write(template.render(template_values))

    def post(self):
        template = JINJA_ENVIRONMENT.get_template('templates/addresource.html')
        template_values = dict()

        user = self.request.get('user')
        url = self.request.get('url')
        url_linktext = self.request.get('url_linktext')


        resource_name = self.request.get('resource_name')
        date_str = self.request.get('start_date')
        date = date_str.split('-')
        start_str = self.request.get('start_time')
        start = start_str.split(':')
        end_str = self.request.get('end_time')
        end = end_str.split(':')
        tags_str = self.request.get('tags')
        capacity = int(self.request.get('capacity'))
        image_flag = self.request.get('image_flag')


        flag = ""
        MSG = ""
        
        start_time = datetime.datetime(int(date[0]), int(date[1]), int(date[2]), int(start[0]), int(start[1]))
        end_time = datetime.datetime(int(date[0]), int(date[1]), int(date[2]), int(end[0]), int(end[1]))
        current_time = getCurrentTime()
        if start_time >= end_time or current_time >= end_time:
            flag = "error"
            MSG = "Please choose valid date and time!"
        else:
            flag = "success"
            MSG = "Resource added!"


        if flag == "success":
            resource = Resources(parent=resource_key())
            resource.resource_id = str(uuid.uuid4())
            resource.resource_owner = user
            resource.resource_name = resource_name
            resource.start_time = start_time
            resource.end_time = end_time
            resource.availability = str(date_str) + ' ' + start[0] + ':' + start[1]+ ' - ' + end[0] + ':' + end[1]
            resource.date_str = date_str
            resource.start_str = start_str
            resource.end_str = end_str
            tags_str = tags_str.split(',')
            resource.tags = [t.strip() for t in tags_str]
            resource.capacity = capacity
            if image_flag == True:
                resource.image_id = str(uuid.uuid4())
                image = self.request.get('image')
                resource.image = image
                resource.avatar = image.resize(avatar, 32, 32)
                resource.description = self.request.get('description')
            resource.reservations = 0
            resource.last_reserve_time = datetime.datetime.min
            resource.put()


        template_values = {
            'user': user,
            'url': url,
            'url_linktext': url_linktext,
            'flag' : flag,
            'msg': MSG,
            'resource_name': resource_name,
            'start_date': date_str,
            'start_time': start_str,
            'end_time': end_str,
            'capacity': capacity,
            'tags': tags_str
        }

        self.response.write(template.render(template_values))
# [END add_resource]

 
# [START edit_resource]
class EditResource(webapp2.RequestHandler):
    def get(self):
        user = self.request.get('user')
        url = users.create_logout_url(self.request.uri)
        url_linktext = 'Logout'

        resource_id = self.request.get('resourceid')
        resources = getPerResource(resource_id)
        resource = resources[0]

        template_values = {
            'user': user,
            'url': url,
            'url_linktext': url_linktext,
            'resource_id': resource_id,
            'resource_name': resource.resource_name,
            'start_date': resource.date_str,
            'start_time': resource.start_str,
            'end_time': resource.end_str,
            'capacity': resource.capacity,
            'tags': ', '.join(resource.tags)
        }

        template = JINJA_ENVIRONMENT.get_template('templates/editresource.html')
        self.response.write(template.render(template_values))


    def post(self):
        template = JINJA_ENVIRONMENT.get_template('templates/editresource.html')
        template_values = dict()

        user = self.request.get('user')
        url = self.request.get('url')
        url_linktext = self.request.get('url_linktext')

        resource_id = self.request.get('resourceid')
        resource_name = self.request.get('resource_name')
        date_str = self.request.get('start_date')
        date = date_str.split('-')
        start_str = self.request.get('start_time')
        start = start_str.split(':')
        end_str = self.request.get('end_time')
        end = end_str.split(':')
        tags_str = self.request.get('tags')
        capacity = int(self.request.get('capacity'))
        image_flag = self.request.get('image_flag')


        flag = ""
        MSG = ""
        
        start_time = datetime.datetime(int(date[0]), int(date[1]), int(date[2]), int(start[0]), int(start[1]))
        end_time = datetime.datetime(int(date[0]), int(date[1]), int(date[2]), int(end[0]), int(end[1]))
        current_time = getCurrentTime()
        if start_time >= end_time or current_time >= end_time:
            flag = "error"
            MSG = "Please choose valid date and time!"
        else:
            flag = "success"
            MSG = "Resource edited!"


        if flag == "success":
            resources = getPerResource(resource_id)
            resource = resources[0]
            resource.resource_name = resource_name
            resource.start_time = start_time
            resource.end_time = end_time
            resource.availability = str(date_str) + ' ' + start[0] + ':' + start[1]+ ' - ' + end[0] + ':' + end[1]
            resource.date_str = date_str
            resource.start_str = start_str
            resource.end_str = end_str
            tags_str = tags_str.split(',')
            resource.tags = [t.strip() for t in tags_str]
            resource.capacity = capacity
            if image_flag == True:
                resource.image_id = str(uuid.uuid4())
                image = self.request.get('image')
                resource.image = image
                resource.avatar = image.resize(avatar, 32, 32)
                resource.description = self.request.get('description')
            resource.reservations = 0
            resource.last_reserve_time = datetime.datetime.min
            resource.put()

            resource_reservations = getResouceReservations(resource_id)
            for r in resource_reservations:
                r.key.delete()


        template_values = {
        'user': user,
        'url': url,
        'url_linktext': url_linktext,
        'flag' : flag,
        'msg': MSG,
        }
        self.response.write(template.render(template_values))
# [END edit_resource]


# [START get_resource_page]
class ResourcePage(webapp2.RequestHandler):
    def get(self):
        user = self.request.get('user')
        url = users.create_logout_url(self.request.uri)
        url_linktext = 'Logout'

        resource_id = self.request.get('resourceid')
        resource = getPerResource(resource_id)
        resource_reservations = getResouceReservations(resource_id)

        template_values = {
            'user': user,
            'url': url,
            'url_linktext': url_linktext,
            'reservations': resource_reservations,
            'resource': resource[0]
        }

        template = JINJA_ENVIRONMENT.get_template('templates/resourcepage.html')
        self.response.write(template.render(template_values))
# [END get_resource_page]


# [START get_tag_page]
class TagPage(webapp2.RequestHandler):
    def get(self):
        user = self.request.get('user')
        url = users.create_logout_url(self.request.uri)
        url_linktext = 'Logout'

        tag = self.request.get('tag')
        resources = []
        all_resources = getAvailableResources()
        for r in all_resources:
            if tag in r.tags:
                resources.append(r)

        template_values = {
            'user': user,
            'url': url,
            'url_linktext': url_linktext,
            'tag': tag,
            'resources': resources
        }

        template = JINJA_ENVIRONMENT.get_template('templates/tagpage.html')
        self.response.write(template.render(template_values))
# [END get_tag_page]


# [START rss_dump]
class RssDump(webapp2.RequestHandler):
    def get(self):
        user = self.request.get('user')
        url = users.create_logout_url(self.request.uri)
        url_linktext = 'Logout'

        resource_id = self.request.get('resourceid')
        resources = getPerResource(resource_id)
        resource = resources[0]
        resource_reservations = getResouceReservations(resource_id)

        rss = []
        rss.append('<?xml version="1.0" encoding="UTF-8" ?>')
        rss.append('<rss version="2.0">')
        rss.append('<resource>')
        rss.append('\t<owner>' + resource.resource_owner + '</owner>')
        rss.append('\t<title>' + resource.resource_name + '</title>')
        rss.append('\t<availability>' + resource.availability + '</availability>')
        rss.append('\t<tags>' + ', '.join(resource.tags) + '</tags>')
        rss.append('\t<capacity>' + str(resource.capacity) + '</capacity>')

        for r in resource_reservations:
            rss.append('\t<reservations>')
            rss.append('\t\t<reserve user>' + r.reservation_user + '</reserve user>')
            rss.append('\t\t<start date>' + r.date_str + '</start date>')
            rss.append('\t\t<start time>' + r.start_time.strftime('%H:%M') + '</start time>')
            rss.append('\t\t<duration>' + r.duration + '</duration>')
            rss.append('\t</reservations>')

        rss.append('</resource>')
        rss.append('</rss>')

        template_values = {
            'user': user,
            'url': url,
            'url_linktext': url_linktext,
            'rss': rss,
            'resource': resource
        }

        template = JINJA_ENVIRONMENT.get_template('templates/rssdump.html')
        self.response.write(template.render(template_values))
# [END rss_dump]


# [START user_page]
class UserPage(webapp2.RequestHandler):
    # 7. the user should be linked to a page that shows all the reservations and resources for a user

    def get(self):
        user = self.request.get('user')
        url = users.create_logout_url(self.request.uri)
        url_linktext = 'Logout'
        user_reservations = getUserReservations(user)
        user_resources = getUserOwnsResources(user)

        current_user = users.get_current_user().email()
        owner_flag = ""
        if current_user == user:
            owner_flag = "owner"

        template_values = {
            'user': user,
            'url': url,
            'url_linktext': url_linktext,
            'user_reservations': user_reservations,
            'user_resources': user_resources,
            'owner_flag': owner_flag
        }

        template = JINJA_ENVIRONMENT.get_template('templates/userpage.html')
        self.response.write(template.render(template_values))
# [END user_page]


# [START main_page]
class MainPage(webapp2.RequestHandler):

    def get(self):

        user = users.get_current_user()
        if not user:
            url = users.create_login_url(self.request.uri)
            self.redirect(url)
        else:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'

            # 2. On the landing page, viewers will see three sections: (1) user's reservations; (2) all resources (3) resources (4) a link to create a new resource.

            # 7. Each reservation should display the user, resource name, reservation time and duration. The resource name should be linked to the resource page
            user_reservations = getUserReservations(user.email())
            available_resources = getAvailableResources()
            user_resources = getUserOwnsResources(user.email())


            template_values = {
                'user': user,
                'url': url,
                'url_linktext': url_linktext,
                'user_reservations': user_reservations,
                'available_resources': available_resources,
                'user_resources': user_resources
            }

            template = JINJA_ENVIRONMENT.get_template('templates/index.html')
            self.response.write(template.render(template_values))
# [END main_page]



# [START app]
app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/searchbyname', SearchByName),
    ('/searchbytime', SearchByTime),
    ('/userpage', UserPage),
    ('/addresource', AddResource),
    ('/editresource', EditResource),
    ('/resourcepage', ResourcePage),
    ('/tagpage', TagPage),
    ('/rssdump', RssDump),
    ('/addreservation', AddReservation),    
    ('/deletereservation', DeleteReservation)
    
], debug=True)
# [END app]
